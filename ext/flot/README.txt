Flot javascript plotting library.

  s3cmd sync --acl-public 0.6/jquery.flot.min.js s3://assets.okfn.org/ext/flot/0.6/
  s3cmd sync --acl-public 0.6/excanvas.min.js s3://assets.okfn.org/ext/flot/0.6/

