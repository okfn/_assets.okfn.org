import os

text2line = 'g3372'
box = 'g2738'
allinone = 'g2778'
images = { 
    'ckan_logo.png': 'g2778',
    'ckan_logo_largetext.png': 'g3620',
    'ckan_logo_box.png': 'g2738',
    'ckan_logo_fullname_long.png': 'fullname_long',
    'ckan_logo_shortname.png': 'g4142',
    'ckan_logo_small_symbol.png': 'small_symbol',
    }

cmdbase = 'inkscape -i=%s -e %s ckan_logo.svg'

print '### Re-exporting svgs to pngs ...'

for name,group in images.items():
    cmd = cmdbase % (group, name)
    print cmd
    os.system(cmd) 

cmd = 'convert -background transparent -gravity center -extent 145x145 ckan_logo_box.png ckan_logo_box_square.png'
os.system(cmd)

print 'Now you need to upload them, e.g.'
print ' scp *.png okfn@m.okfn.org:~/hg-m.okfn.org/gfx/ckan_logo/'


