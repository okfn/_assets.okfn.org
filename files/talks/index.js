var OKFN_TALKS_LIST = {
  "activate_20100701": {
    "date": "2010-07-01", 
    "event": "", 
    "location": "", 
    "title": "Where Does My Money Go? - Digging into Public Spending - Activate 2010"
  }, 
  "british_academy_20070330": {
    "date": "2007-03-30", 
    "event": "", 
    "location": "", 
    "title": "The Need for More Openness -\n    Presentation at the AHRC/British Academy Conferrence on Copyright and Research in the Humanities and Social Sciences Conference"
  }, 
  "budapest_open_data_workshop_20110519": {
    "date": "2011-05-19", 
    "event": "", 
    "location": "Budapest, Hungary", 
    "title": "Budapest, 2011-05-19 - Open (Government) Data: What, Why, How"
  }, 
  "budapest_workshop_20110518": {
    "date": "2011-05-13", 
    "event": "", 
    "location": "", 
    "title": "Budapest, 2011-05-13 - Open (Government) Data: What, Why, How"
  }, 
  "ccc_20091228": {
    "date": "2009-12-28", 
    "event": "", 
    "location": "", 
    "title": "Chaos Computer Congress 2009: December 28th 2009 - CKAN and Building the Debian of Data"
  }, 
  "ccsr_manchester_20110125": {
    "date": "2011-01-25", 
    "event": "", 
    "location": "", 
    "title": "CCSR, Manchester, 2011-01-25 - Open Data"
  }, 
  "cgn_20110214": {
    "date": "2011-02-14", 
    "event": "", 
    "location": "", 
    "title": "Cambridge Geek Night 2011-02-14 - Open Data"
  }, 
  "communia_20080118": {
    "date": "2008-01-18", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge: Promises and Challenges"
  }, 
  "consegi_brasilia_20110511": {
    "date": "2011-05-11", 
    "event": "", 
    "location": "", 
    "title": "CONSEGI, Brasilia, 2011-05-11 - Open (Government) Data: What, Why, How"
  }, 
  "copyright_1710-2010_20100409": {
    "date": "2010-04-09", 
    "event": "", 
    "location": "", 
    "title": "Open Public Data: What, Why and What's Happening"
  }, 
  "data-insights_digging-into-data_20120202": {
    "date": "2012-02-02", 
    "event": "Data Insights Meetup", 
    "location": "Cambridge, UK", 
    "title": "Digging into Open Data"
  }, 
  "data.gv.at_vienna_20100408": {
    "date": "2010-04-08", 
    "event": "", 
    "location": "", 
    "title": "Open (Public) Data: What, Why, How"
  }, 
  "dataconomy_20101021": {
    "date": "2010-10-21", 
    "event": "", 
    "location": "", 
    "title": "dataconomy 2010-10-21 - Open Data"
  }, 
  "distributed_data_20100423": {
    "date": "2010-04-23", 
    "event": "", 
    "location": "", 
    "title": "Distributed Versioned Data Development"
  }, 
  "economies_of_the_commons_20101113": {
    "date": "2010-11-13", 
    "event": "", 
    "location": "", 
    "title": "Economics of the Commons 2010 - Open Data and Open Data Tools"
  }, 
  "estc_vienna_lod_20101202": {
    "date": "2010-12-02", 
    "event": "", 
    "location": "", 
    "title": "Semantic Web and Open Data"
  }, 
  "etech_2006": {
    "date": "", 
    "event": "", 
    "location": "", 
    "title": "ETech '06: Hack Your Own Conference"
  }, 
  "festival_economia_trento_20100606": {
    "date": "2010-06-06", 
    "event": "", 
    "location": "", 
    "title": "Open Public Data: What, Why, How"
  }, 
  "finnish_institute_20100429": {
    "date": "2010-04-29", 
    "event": "", 
    "location": "", 
    "title": "Open Public Data: What, Why, How"
  }, 
  "forum_virium_open_city_20100311": {
    "date": "2010-03-11", 
    "event": "", 
    "location": "", 
    "title": "Forum Virium - Open Up the City"
  }, 
  "global_interoperability_linked_data_dpla_20110516": {
    "date": "2011-05-16", 
    "event": "", 
    "location": "", 
    "title": "Global Interoperability and Linked Data and the DPLA - 2011-05-16 - Amsterdam"
  }, 
  "hacks_and_hacking_20100713": {
    "date": "2010-07-13", 
    "event": "", 
    "location": "", 
    "title": "Where Does My Money Go? - Digging into Public Spending - Hacks and Hacking"
  }, 
  "icaew_open_data_20111110": {
    "date": "2011-11-10", 
    "event": "ICAEW IT Faculty Annual Lecture", 
    "location": "London, UK", 
    "title": "Open Data: What, Why, How"
  }, 
  "jisc_jif_20100728": {
    "date": "2010-07-28", 
    "event": "", 
    "location": "", 
    "title": "Open Data"
  }, 
  "kforge_20100423": {
    "date": "2010-04-23", 
    "event": "", 
    "location": "", 
    "title": "KForge - Open Project Hosting"
  }, 
  "lapsi_bocconi_community_and_technology_20110505": {
    "date": "2011-05-05", 
    "event": "", 
    "location": "", 
    "title": "LAPSI 2011 - Community and Openness"
  }, 
  "lapsi_bocconi_psi_costs_and_benefits_openness_20110505": {
    "date": "2011-05-05", 
    "event": "", 
    "location": "", 
    "title": "LAPSI 2011 - Public Sector Information (PSI) - Costs and Benefits of Openness"
  }, 
  "law2.0_20070917": {
    "date": "2007-09-17", 
    "event": "", 
    "location": "", 
    "title": "Openness, Web 2.0 and the Ethic of Sharing -\n    Presentation at SCL's Law 2.0 Conference"
  }, 
  "lift2012_open-data_20120224": {
    "date": "2012-02-24", 
    "event": "LIFT 2012", 
    "location": "Geneva, Switzerland", 
    "title": "Open Data: Where We Are, Where We Are Going"
  }, 
  "lugradio_20080719": {
    "date": "2008-07-19", 
    "event": "", 
    "location": "", 
    "title": "LUGRadio Live 2008: July 19th 2008 - Open Data the Debian Way"
  }, 
  "managing_public_sector_information_20110301": {
    "date": "2011-03-01", 
    "event": "", 
    "location": "", 
    "title": "Opening Data to the Public Data"
  }, 
  "manchester_datagm_20101122": {
    "date": "2010-11-22", 
    "event": "", 
    "location": "", 
    "title": "DataGM - Manchester Open Data"
  }, 
  "odf_plugfest_20110225": {
    "date": "2011-02-25", 
    "event": "", 
    "location": "", 
    "title": "ODF Plugest - Open Data"
  }, 
  "ogd_austria_2011": {
    "date": "2011-06-16", 
    "event": "Open Government Data Conference", 
    "location": "Vienna, Austria", 
    "title": "Open Government Data Conference - Open Data: Where Next?"
  }, 
  "ogd_bern_20110624": {
    "date": "2011-06-24", 
    "event": "Open Government Data Conference", 
    "location": "Bern, Switzerland", 
    "title": "Open Data: What, Why, How and Where Next?"
  }, 
  "ogdcamp_20101118": {
    "date": "2010-11-18", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge Foundation - A Year in Review"
  }, 
  "ogdcamp_2011": {
    "date": "2011-10-20", 
    "event": "OGDCamp", 
    "location": "Warsaw, Poland", 
    "title": "Open Data, Open Tools, Open Community"
  }, 
  "ogn_20080206": {
    "date": "2008-02-06", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge 1.0: March 17 2007"
  }, 
  "okcon_year_in_review_20100424": {
    "date": "2010-04-24", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge Foundation - A Year in Review"
  }, 
  "okf_intro": {
    "date": "", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge Foundation - An Introduction"
  }, 
  "okscotland_20100513": {
    "date": "2010-05-13", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge Foundation - An Introduction"
  }, 
  "ona2010_20101030": {
    "date": "2010-10-30", 
    "event": "", 
    "location": "", 
    "title": "Where Does My Money Go? - Digging into Public Spending - ONA 2010"
  }, 
  "open_access_cam_20091021": {
    "date": "2009-10-21", 
    "event": "", 
    "location": "", 
    "title": "Open Access - Why It Matters"
  }, 
  "open_data_semantic_web_20091113": {
    "date": "2009-11-13", 
    "event": "", 
    "location": "", 
    "title": "Semantic Web and Open Data"
  }, 
  "open_eindhoven_20110610": {
    "date": "2011-06-10", 
    "event": "", 
    "location": "Eindhoven, Netherlands", 
    "title": "Open Data: What, Why, How - Eindhoven, 2011-06-10"
  }, 
  "open_licensing_picnic_2011": {
    "date": "2011-09-15", 
    "event": "PICNIC", 
    "location": "Amsterdam, Netherlands", 
    "title": "Open Licensing and Open Data"
  }, 
  "open_shakespeare_british_library_20110224": {
    "date": "2011-02-24", 
    "event": "", 
    "location": "", 
    "title": "Open Shakespeare 2011-02-24 - British Library"
  }, 
  "open_shakespeare_british_libray_20110224": {
    "date": "2011-02-24", 
    "event": "", 
    "location": "", 
    "title": "Open Shakespeare 2011-02-24 - British Library"
  }, 
  "openeverything_20081106": {
    "date": "2008-11-06", 
    "event": "", 
    "location": "", 
    "title": "The Value of Openness -\n    Presentation at Open Everything"
  }, 
  "opengovuk_pressconf_20101119": {
    "date": "2010-11-19", 
    "event": "", 
    "location": "", 
    "title": "Where Does My Money Go? - Digging into Public Spending - ONA 2010"
  }, 
  "openness_and_libraries_20100127": {
    "date": "2010-01-27", 
    "event": "", 
    "location": "", 
    "title": "Openness and Libraries"
  }, 
  "opentech_20080705": {
    "date": "2008-07-05", 
    "event": "", 
    "location": "", 
    "title": "OpenTech 2008: July 5th 2008 - Opening Data"
  }, 
  "opentech_20090704": {
    "date": "2009-07-04", 
    "event": "", 
    "location": "", 
    "title": "Opening Up Government Data - Give it to Us Raw, Give it to us Now"
  }, 
  "opentech_20100911_data.gov.uk": {
    "date": "2010-09-11", 
    "event": "", 
    "location": "", 
    "title": "Data.Gov.Uk A Year On - OpenTech 2010"
  }, 
  "opentech_20100911_wdmmg": {
    "date": "2010-09-11", 
    "event": "", 
    "location": "", 
    "title": "Where Does My Money Go? - Digging into Public Spending - OpenTech 2010"
  }, 
  "opentech_20110521": {
    "date": "2011-05-21", 
    "event": "", 
    "location": "", 
    "title": "OpenSpending - Where Does My Money Go Goes Global"
  }, 
  "orgcon_20100724": {
    "date": "2010-07-24", 
    "event": "", 
    "location": "", 
    "title": "Open Data"
  }, 
  "picnic10_open_data_20100924": {
    "date": "2010-09-24", 
    "event": "", 
    "location": "", 
    "title": "Picnic 2010 - Open Data"
  }, 
  "practicalaction_20081124": {
    "date": "2008-11-24", 
    "event": "", 
    "location": "", 
    "title": "How Open Knowledge Delivers Value -\n    Delivering Public Value from New Technologies"
  }, 
  "republica_open_data_20110413": {
    "date": "2011-04-13", 
    "event": "", 
    "location": "", 
    "title": "RE:PUBLICA, Berlin, 2011-04-13 - Open (Government) Data: What, Why, How"
  }, 
  "rewiredstate_20090307": {
    "date": "2009-03-07", 
    "event": "", 
    "location": "", 
    "title": ""
  }, 
  "rootstock_20090523": {
    "date": "2009-05-23", 
    "event": "", 
    "location": "", 
    "title": "The Benefits of Openness -\n    Presentation at Practical Economics"
  }, 
  "royal_statistical_society_20100607": {
    "date": "2010-06-07", 
    "event": "", 
    "location": "", 
    "title": "Royal Statistical Society"
  }, 
  "scata_20100510": {
    "date": "2010-05-10", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge and Data: What, Why, How"
  }, 
  "seurat_open_data_20110509": {
    "date": "2011-05-09", 
    "event": "", 
    "location": "", 
    "title": "SEURAT - Open Data - What, Why, How"
  }, 
  "shuttleworth_gathering_20110620": {
    "date": "2011-06-20", 
    "event": "Shuttleworth Gathering", 
    "location": "Geneva, Switzerland", 
    "title": "An Intro to the Open Knowledge Foundation"
  }, 
  "shuttleworth_update_20110117": {
    "date": "2011-01-17", 
    "event": "", 
    "location": "", 
    "title": "Updates"
  }, 
  "sofia_open_camp_20110605": {
    "date": "2011-06-05", 
    "event": "", 
    "location": "Sofia, Bulgaria", 
    "title": "Sofia, 2011-06-05 - Open Data: What, Why, How"
  }, 
  "topix_turin_20101203": {
    "date": "2010-12-03", 
    "event": "", 
    "location": "", 
    "title": "TOP-IX, Turin, 2010-12-03 - Open Data"
  }, 
  "uksg_open_bibliography_20110405": {
    "date": "2011-04-05", 
    "event": "", 
    "location": "", 
    "title": "UKSG 2011 - Open Bibliography"
  }, 
  "xtech_2007": {
    "date": "", 
    "event": "", 
    "location": "", 
    "title": "Open Knowledge 1.0: March 17 2007"
  }
};