<?xml version="1.0" encoding="UTF-8"?>
<html
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:doc="http://www.witbd.org/xmlns/common/document/"
	xmlns:st="http://www.witbd.org/xmlns/text/structure"
	xmlns:te="http://www.witbd.org/xmlns/text/basic/"
	xmlns:bib="http://www.witbd.org/xmlns/biblio/"
	xmlns:ch="http://xmlchar.sf.net/ns#"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dct="http://purl.org/dc/terms/"
	xmlns:mml="http://www.w3.org/1998/Math/MathML"
	>
<head>
	<doc:Metadata>
		<rdf:RDF
			xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
			xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
			xmlns:dc="http://purl.org/dc/elements/1.1/"
			xmlns:dct="http://purl.org/dc/terms/">
			<rdf:Description rdf:about="">
				<dc:title>The Medical Innovation Convention: A new global framework for Healthcare Research and Development</dc:title>
				<dc:creator>Rufus Pollock</dc:creator>
				<dct:created>2004-10-30</dct:created>
				<dct:modified></dct:modified>
				<rdfs:comment xml:lang="en-GB"></rdfs:comment>
			</rdf:Description>  
		</rdf:RDF>
	</doc:Metadata>
	<doc:License>
		<rdf:RDF xmlns="http://web.resource.org/cc/"
    	xmlns:dc="http://purl.org/dc/elements/1.1/"
			xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
			
			<Work rdf:about="">
				<dc:type rdf:resource="http://purl.org/dc/dcmitype/Text" />
				<license rdf:resource="http://creativecommons.org/licenses/by/2.0/" />
			</Work>
			
			<License rdf:about="http://creativecommons.org/licenses/by/2.0/">
				<permits rdf:resource="http://web.resource.org/cc/Reproduction" />
				<permits rdf:resource="http://web.resource.org/cc/Distribution" />
				<requires rdf:resource="http://web.resource.org/cc/Notice" />
				<requires rdf:resource="http://web.resource.org/cc/Attribution" />
				<permits rdf:resource="http://web.resource.org/cc/DerivativeWorks" />
			</License>
		</rdf:RDF>
	</doc:License>
</head>
	
<body>
<h1>
	The Medical Innovation Convention: A New Global Framework for Healthcare Research and Development
</h1>

<st:TableOfContents />

<st:Section>
	<h2>
		Introduction
	</h2>
	<p>
		Funding research and development for new drugs and treatments as well as the transfer of existing knowledge around the globe is essential to improving healthcare in the twenty-first century, especially for those in developing countries.
	</p>
	<p>
		However the current global innovation system for healthcare, heavily based on TRIPs and ever stronger intellectual property rights, is in crisis, failing to deliver, not only for developing countries but also, increasingly, for high-income nations. As a solution to this growing problem the Medical Innovation Convention proposes a new, more efficient, approach to incentivizing and producing medical innovation.
	</p>
</st:Section>
<st:Section>
	<h2>
		Financing Medical Innovation: The Economics
	</h2>
	<p>
		 To understand the issues surrounding the funding of medical R&amp;D it is necessary to look at the economics very briefly. In economist's jargon R&amp;D is a public good, that is, what is produced by a single group or individual may be used by all. This suggests that if unregulated there would be very significant under-provision of R&amp;D as all participants would have an incentive to do nothing and simply 'free-ride' on the efforts of others. For example if a new vaccine costs a $100 million to develop but can be produced for almost nothing everyone will hope that someone else will bear the cost of the research at which point they get the vaccine for free.
	</p>
	<p>
		Thus to organize an optimal level of spending on R&amp;D it is necessary to have some way of stopping this free-riding. There are two main approaches:
	</p>
	<ol>
		<li>
			Coordinate contributions. For example through government funded research that is paid for by all taxpayers.
		</li>
		<li>
			Create monopolies in innovations using intellectual property rights such as patents. By making an innovation <em>excludable</em> patents allow its owner to force users to pay thereby providing for a means to recoup the costs of development.
		</li>
	</ol>
	<p>
		Both systems are currently used throughout the world, for example in the US the National Institute of Health (NIH) spends approx $27 billion a year while at the same time almost all new clinical drugs are patented and sold at monopoly prices. The problem is that the patent monopoly system, embedded as it is into the WTO TRIPS agreement, has come to dominate discussions of drug pricing and medical R&amp;D at an international level. This is problematic because:
	</p>
	<blockquote>
		<p>
			Marketing monopolies are ... inefficient. Only a small fraction of the high prices is reinvested in research and development, and most of this on non-innovative 'me too' products for chronic diseases that afflict high-income patients [it is estimated that about 10-15% of drug sales goes back into to R&amp;D]. Very little private R&amp;D is invested in basic research, public goods such as the Human Genome Project (HGP) or Medline, the development of vaccines, or higher priority medicines, such as new treatments for malaria. Higher IPR protection for products is also associated with a number of other problems, including excessive secrecy and anti-competitive barriers to follow-on innovation.
		</p>
		<p>
			The massive investments in marketing medicines protected by patents and other exclusive rights are not only wasteful, they are also often associated with inappropriate use of products arising from fraudulent or unethical practices that skew the evidence and incentives that determine which medicines are prescribed. [Love and Hubbard 2004]
		</p>
	</blockquote>
	
	<st:Section>
		<h3>
			The Medical Innovation Convention
		</h3>
		<p>
			The Medical Innovation Convention proposes solving the free-rider problem by coordination instead of by monopoly IP rights. The Convention sets out a scheme by which each signatory country commits to do a set amount of medical R&amp;D per year (dependent on wealth). In this way the Convention seeks to decouple the end, funding of R&amp;D, from the means by which this achieved. For example in the current draft a country could meet its R&amp;D commitments by, among other methods, buying patented pharmaceuticals, government or charitably funded work, and innovation 'prizes'.
		</p>
		<p>
			To give an example of what this flexibility means consider a hypothetical country named Kanukatai. Let us suppose that the calculated contribution rates are: 15% for patented medical products, 85% for innovation prizes and 100% for direct government spending (these numbers are intended to reflect the proportion of money spent that actually results in R&amp;D). Suppose also that the Kanukataians spend $1 billion on patented medical products, and their Government allocates another $1 billion in prizes and another $1 billion in direct research. Then the Kanukatai will have spend a weighted total of $2 billion on medical innovation. This would then be compared against their obligations under the Convention and any net position resolved in a suitable manner (for example contribution to a central fund).
		</p>
		<p>
			The convention also attempts to deal with the skewed priorities of current healthcare R&amp;D. It seeks to provide a method by which projects with high public returns but low private ones will be funded. This is done in in a subsection relating to, so-called, 'priority' medical research which seeks to target currently underfunded areas such as vaccine development, open public databases and research tools, and the preservation and dissemination of traditional medical knowledge.
		</p>
	</st:Section>
</st:Section>
<st:Section>
	<h2>
		References and Further Information
	</h2>
	<p>
		<strong>love_ea_2004</strong>
		<em>Make Drugs Affordable: Replace TRIPs-plus by R&amp;D-plus</em>; Love, J.; Hubbard T; Bridges June 2004
		<a href="http://www.ictsd.org/monthly/bridges/BRIDGES8-6.pdf">
			http://www.ictsd.org/monthly/bridges/BRIDGES8-6.pdf
		</a>
	</p>
	<p>
		<strong>mic</strong> <em>Medical Innovation Convention</em>; Current working drafts of the Medical Innovation Convention (also known as the 'Big' treaty) may be found at
		<a href="http://www.cptech.org/workingdrafts/rndtreaty.html">
			http://www.cptech.org/workingdrafts/rndtreaty.html
		</a>
	</p>
</st:Section>
</body>
</html>
