

The Euro Directive SI 2005 (1515) Re-Use of Public Sector Information.

The Euro Directive on the Re Use of Public Sector Information came into
force in the UK on the 1st July 2005 with the publication of Statutory
Instrument SI 2005 (1515).

The main objective of the directive is to promote the positive re-use of
public sector information held by public sector organisations within the UK
to promote the value of the information/knowledge economy (Lisbon Agenda).

It was intended that the Regulations would lead to the following
deliverables in terms of primary objectives:-

  * Identification of Public Sector documents that were available for re-
    use
  * Availability of documentation at marginal cost
  * Clarity & transparency of charging procedures
  * Processing of applications in an open and timely manner
  * Application of fair, consistent and non-discriminatory processes.
  * Acceptance of the need for open terms , conditions and licences.
  * Sharing of ethos of Public Sector Best Practice
  * Establishment of complaints & redress processes.

Within the UK system, the primary & possibly the most obvious interface
between Public & Private Sectors in the Economy is actively managed by
Local Government in the form of the Local Authorities who are split
regionally & geographically into 426 County, District, Unitary &
Metropolitan Councils throughout England and Wales.

For the Euro Directive to have any chance of success in the promotion of
the information economy & wider objectives - there is an inherent need for
widespread acceptance within the public sector (and particularly within
Local Authorities) where much of the citizen centric machinery of
Government is centred & applied.

To that effect, the acceptance & promotion of the objectives of the Euro
Directive at Local Authority level(s) in the UK are fundamental to the
whole process of governance and in order to chart the progress of the
Directive some 20 months after inception - I have undertaken an exhaustive
survey of all 426 Local Authorities seeking their views on the Euro
Directive in terms of:

  * Compilation of Asset Lists of Information available for re-use.
  * Procedures for requesting that information.
  * Charges, Terms & Conditions which would apply.
  * Details of Application process for Re-Use Licences.

The replies received from the Local Authorities are disturbing in that:

  * Over 40% have failed to offer the simple courtesy of a reply.
  * 75% of those who have replied - have FAILED to comply with the Euro
      Directive.
  * On the part of Local Authorities there seems to be a very high degree
      of confusion between the management of FOI (Freedom of Information)
      and the re-use of PSI (Public Sector Information).

Having had the opportunity to speak to a number of Information Officers
within those Local Authorities who found the time to respond to the survey,
it seems obvious that whilst individual Officers were generally helpful,
the whole acceptance & management of the needs of the Euro Directive on the
Re-Use of Public Sector Information were beset with problems in terms of:

  * A defined lack of Understanding of the distinction between FOI & PSI.
  * Poor leadership in terms of prioritisation.
  * Lack of available resources to enable compliance
  * Common misconception that existing FOI Publication Schemes met the
      needs of an active Asset Register of information available for re-use.

The Survey of all 426 Local Authorities clearly indicates that although
Central Government resources have been allocated to promoting the needs of
the Euro Directive - through OPSI (Office of Public Sector Information);
APPSI (Advisory Panel on Public Sector Information) and the Office of the
Information Commissioner, acceptance at an operational level with UK Local
Authorities is at best poor and at worst - non existent.

The Euro Directive is to be revisited in 2008 and if compliance is to be
achieved there will need to be a sea change of attitude from within the
Public Sector and particularly Local Authorities as a focal point of local
governance within the UK.

A failure to establish meaningful Re-use regimes will not only continue to
undermine the objectives of the 2005 Directive but will also impede &
obstruct both public & private sector initiatives and lead to a widespread
failure in the UK to both manage and promote the inherent value of the
information economy as a positive & beneficial resource.





