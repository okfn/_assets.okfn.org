## Open Knowledge and Open Data Web Buttons

If making new ones please create in the svg file (ok_buttons.svg) and then
export.

### Notes on Fabrication

  * Uses Jason Kottke's silkscreen font.
  * Must set dpi=90 in inkscape
  * Set snap-to-grid in inkscape and ensure everything you create starts and
    ends at integer pixel coordinates. Otherwise png export will be blurry as
    inkscape will do anti-aliasing.

### Online Services

  * http://www.yugatech.com/make.php
  * http://www.lucazappa.com/brilliantMaker/buttonImage.php
