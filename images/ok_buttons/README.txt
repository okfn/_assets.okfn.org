## Open Knowledge and Open Data Web Buttons

If making new ones please create in the svg file (ok_buttons.svg) and then
export.

Nick says (June 1, 2011): many (most) of these images had issues with anti-aliasing, either in the text or in the fill of background colours. This has now been fixed, and 80x15 and 80x23 versions of each word combination and colour scheme have been updated. The new versions are more readable, and are even smaller in file size.

### Notes on Fabrication

  * Uses Jason Kottke's "Silkscreen" font, at 8pt.
  * Three source files generate the multiple colour variations of the three main sizes (80x15, 80x23, and 90x15 for "Open Knowledge", which looks a bit cramped in the 80 pixel version).
  * Please, if possible, use these PSDs, as you will (hopefully) be less likely to end up with antialiased fonts and badly rasterized infills.

Nick considers the following deprecated advice:

  * Must set dpi=90 in inkscape
  * Set snap-to-grid in inkscape and ensure everything you create starts and
    ends at integer pixel coordinates. Otherwise png export will be blurry as
    inkscape will do anti-aliasing.

### Online Services

  * http://www.yugatech.com/make.php
  * http://www.lucazappa.com/brilliantMaker/buttonImage.php
