## Fonts

* HelveticaNeueLTStd-Roman.otf: font needed for logo lettering

## Standard White and Green Logo (O with K and F inside) as Bitmaps

* okf_logo_white_and_green.xcf: master
  * pure bitmap (so no way to change lettering)
* okf_logo_white_and_green.jpg: exported version of master
* okf_logo_white_and_green.png: ditto
* okf_logo_bw.png: black and white version of the logo
  * okf_logo_bw_hres.png: 600dpi, 125mmx125mm version
* favicon.ico: favicon version of this logo.

USEFUL: you can get a tiny (png) version of this logo (for use in docs or
elsewhere) by converting the favicon.ico back to png (e.g. with
imagemagick/convert installed do::

    $ convert favicon.ico favicon.png


## SVG versions of logo (different colours and formats)

* okf_logos.svg

  * Lettering requires HelveticaNeueLTStd-Roman.otf

  * Drop shadow not quite as nice currently but svg which makes it easier to
    make multiple versions with different colours and backgrounds
