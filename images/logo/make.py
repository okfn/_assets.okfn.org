import os

def bw_logo():
    fn = 'okf_logo_bw_hres.orig.png'
    outfn = 'okf_logo_bw_hres.png'
    outfn2 = 'okf_logo_wb_hres.png'
    cmd = 'convert %s -transparent white %s' % (fn, outfn)
    os.system(cmd)
    cmd = 'convert -negate %s %s' % (outfn, outfn2)
    os.system(cmd)

bw_logo()
