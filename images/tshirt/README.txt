## Design 1

Want it printed on a **black** t-shirt.

Use ../logo/okf_logo_bw_hres.png (though need to invert the colours as here the logo is black on white).

Back text should read (properly centered):

    The Open Knowledge Foundation
        http://www.okfn.org/

      Promoting Open Knowledge
         In a Digital Age

